
#ENV http_proxy=http://wwwcache.univ-orleans.fr:3128
#ENV https_proxy=https://wwwcache.univ-orleans.fr:3128

FROM node:carbon

WORKDIR /app
COPY package.json /app


RUN npm install

COPY . /app

CMD node app.js
EXPOSE 4000

