# Docker node js

### Installation avec Docker compose 

```
docker-compose build
```
Cette commande créera les containers et le dossier node_modules

### Lancement avec docker compose

```
docker-compose up
```
Cette commande lance le serveur node js sur le port 4000

### Accès à la page en local

Il suffit ensuite d'ouvrir un navigateur et d'aller à l'adresse :
```
localhost:4000
```

