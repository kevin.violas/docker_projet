const express = require("express");
let session = require("cookie-session");
const bodyParser = require("body-parser");
let urlencodedParser = bodyParser.urlencoded({ extended: false});
const jade = require("jade");
let app = express();
const port = 4000;
var json2csv = require('json2csv');
var fs = require('fs');
var jsonexport = require('jsonexport');

const MongoClient = require('mongodb').MongoClient;
var autoIncrement = require("mongodb-autoincrement");
const assert = require('assert');

// Connection URL
const uri = 'mongodb://kevinviolas:wsjE75KQH8K4289v@cluster0-shard-00-00-vqv1b.mongodb.net:27017,cluster0-shard-00-01-vqv1b.mongodb.net:27017,cluster0-shard-00-02-vqv1b.mongodb.net:27017/node_app?ssl=true&replicaSet=Cluster0-shard-0&authSource=admin';

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.use(session({secret: "kviolas"}))
.use(function(req, res, next) {
    if(typeof(req.session.node_app) == "undefined") {
        req.session.node_app = []
    }
    next();
})
.get("/app", function(req, res) {
  MongoClient.connect(uri, function(err, db) {
    if(err) throw err;
    let collection = db.collection("node_app");
    collection.find({}).toArray(function(err, resultat) {
      if (err) throw err;
      res.render('app.jade', {node_app: resultat});
    })
    db.close();
  });
})
.get("/app/asc_name", function(req, res) {
  MongoClient.connect(uri, function(err, db) {
    if(err) throw err;
    let collection = db.collection("node_app");
    let mysort = {name: 1}
    collection.find({}).sort(mysort).toArray(function(err, resultat) {
      if (err) throw err;
      res.render('app.jade', {node_app: resultat, order_name: "asc"});
    })
    db.close();
  });
})
.get("/app/desc_name", function(req, res) {
  MongoClient.connect(uri, function(err, db) {
    if(err) throw err;
    let collection = db.collection("node_app");
    let mysort = {name: -1}
    collection.find({}).sort(mysort).toArray(function(err, resultat) {
      if (err) throw err;
      res.render('app.jade', {node_app: resultat, order_name: "desc"});
    })
    db.close();
  });
})
.get("/app/asc_type", function(req, res) {
  MongoClient.connect(uri, function(err, db) {
    if(err) throw err;
    let collection = db.collection("node_app");
    let mysort = {type: 1}
    collection.find({}).sort(mysort).toArray(function(err, resultat) {
      if (err) throw err;
      res.render('app.jade', {node_app: resultat, order_type: "asc"});
    })
    db.close();
  });
})
.get("/app/desc_type", function(req, res) {
  MongoClient.connect(uri, function(err, db) {
    if(err) throw err;
    let collection = db.collection("node_app");
    let mysort = {type: -1}
    collection.find({}).sort(mysort).toArray(function(err, resultat) {
      if (err) throw err;
      res.render('app.jade', {node_app: resultat, order_type: "desc"});
    })
    db.close();
  });
})
.get("/app/export_csv", function(req, res) {
  MongoClient.connect(uri, function(err, db) {
    if(err) throw err;
    let collection = db.collection("node_app");
    let mysort = {name: 1}
    collection.find({}).sort(mysort).toArray(function(err, resultat) {
      if (err) throw err;
      let fields = ['name', 'type'];
      var csv = json2csv({ data: resultat, fields: fields });

      fs.writeFile('pref.csv', csv, function(err) {
        if (err) throw err;
        console.log('file saved');
      });
      res.render('app.jade', {node_app: resultat, order_type: "desc"});
    })
    db.close();
  });
})
.get("/app/export_json", function(req, res) {
  MongoClient.connect(uri, function(err, db) {
    if(err) throw err;
    let collection = db.collection("node_app");
    let mysort = {name: 1}
    collection.find({}).sort(mysort).toArray(function(err, resultat) {
      if (err) throw err;
      jsonexport(resultat,function(err, csv){
        if(err) return console.log(err);
        
        fs.writeFile('pref.json', csv, function(err) {
          if (err) throw err;
          console.log('file saved');
        });
      });
      res.render('app.jade', {node_app: resultat, order_type: "desc"});
    })
    db.close();
  });
})
.post("/app/ajouter", function(req, res) {
    MongoClient.connect(uri, function(err, db) {
      if(err) throw err;
      console.log("[INFO] - Connection is established...");
      let collectionName = 'node_app';
      autoIncrement.getNextSequence(db, collectionName, function (err, autoIndex) {
        let collection = db.collection(collectionName);
        if(req.body.type == "Film") {
          console.log(req.body.name)
          var myobj = { name: req.body.name, type: req.body.type, real: '', date: '', duree: '', id: autoIndex};
          collection.insertOne(myobj, function(err, resultat) {
            if (err) throw err;
            console.log("1 film inserted");
          })
        }
        else if(req.body.type == "Série") {
          var myobj = { name: req.body.name, type: req.body.type, real: '', date: '', saison: '', episodes: '', id: autoIndex};
          collection.insertOne(myobj, function(err, resultat) {
            if (err) throw err;
            console.log("1 série inserted");
          })
        }
        else {
          var myobj = { name: req.body.name, type: req.body.type, artiste: '', date: '', genre: '', id: autoIndex};
          collection.insertOne(myobj, function(err, resultat) {
            if (err) throw err;
            console.log("1 série inserted");
          })
        }

        collection.find({}).toArray(function(err, resultat) {
          if (err) throw err;
          res.render('app.jade', {node_app: resultat});
        })
      });
    });
})
.post("/app/modifier/:index", function(req, res) {
  MongoClient.connect(uri, function(err, db) {
    if(err) throw err;
    let collection = db.collection("node_app");
    let index = parseInt(req.params.index);
    let query = {id: index}
    let newvalues = {name: req.body.newval, type: req.body.type, id: index};
    collection.updateOne(query, newvalues, function(err, res) {
      if(err) throw err;
      console.log("1 document updated")
    });

    collection.find({}).toArray(function(err, resultat) {
      if (err) throw err;
      res.render('app.jade', {node_app: resultat});
    });

    db.close();
  });
})
.get("/app/supprimer/:index", function(req,res) {
    MongoClient.connect(uri, function(err, db) {
      if(err) throw err;
      let collection = db.collection("node_app");
      let index = parseInt(req.params.index);
      let query = {id: index};
      collection.deleteOne(query, function(err, resultat) {
        if (err) throw err;
        console.log("1 document deleted");
      })

      collection.find({}).toArray(function(err, resultat) {
        if (err) throw err;
        res.render('app.jade', {node_app: resultat});
      })

      db.close();
    });
})
.get("/app/consulter/:index", function(req, res) {
  MongoClient.connect(uri, function(err, db) {
    if(err) throw err;
    let collection = db.collection("node_app");
    let index = parseInt(req.params.index);
    collection.find({id:index}).toArray(function(err, resultat) {
      if (err) throw err;
      res.render('consult.jade', {node_app: resultat});
    })
    db.close();
  });
})
.post("/app/modifier_film/:index", function(req, res) {
  MongoClient.connect(uri, function(err, db) {
    if(err) throw err;
    let collection = db.collection("node_app");
    let index = parseInt(req.params.index);
    let query = {id: index}
    let newvalues = {name: req.body.name, type: "Film", real: req.body.real, date: req.body.date, duree: req.body.duree, id: index};
    collection.updateOne(query, newvalues, function(err, res) {
      if(err) throw err;
      console.log("1 document updated")
    });

    collection.find({id:index}).toArray(function(err, resultat) {
      if (err) throw err;
      res.render('consult.jade', {node_app: resultat});
    });

    db.close();
  });
})
.post("/app/modifier_serie/:index", function(req, res) {
  MongoClient.connect(uri, function(err, db) {
    if(err) throw err;
    let collection = db.collection("node_app");
    let index = parseInt(req.params.index);
    let query = {id: index}
    let newvalues = {name: req.body.name, type: "Série", real: req.body.real, date: req.body.date, saison: req.body.saison, episodes: req.body.episodes, id: index};
    collection.updateOne(query, newvalues, function(err, res) {
      if(err) throw err;
      console.log("1 document updated")
    });

    collection.find({id:index}).toArray(function(err, resultat) {
      if (err) throw err;
      res.render('consult.jade', {node_app: resultat});
    });

    db.close();
  });
})
.post("/app/modifier_musique/:index", function(req, res) {
  MongoClient.connect(uri, function(err, db) {
    if(err) throw err;
    let collection = db.collection("node_app");
    let index = parseInt(req.params.index);
    let query = {id: index}
    let newvalues = {name: req.body.name, type: "Musique", artiste: req.body.artiste, date: req.body.date, genre: req.body.genre, id: index};
    collection.updateOne(query, newvalues, function(err, res) {
      if(err) throw err;
      console.log("1 document updated")
    });

    collection.find({id:index}).toArray(function(err, resultat) {
      if (err) throw err;
      res.render('consult.jade', {node_app: resultat});
    });

    db.close();
  });
})
.use(function(req, res, next) {
  res.writeHead(302,  {Location: "/app"})
  res.end()
})

app.listen(port, function() {
    console.log("Serveur à l'écoute sur le port %d",port)
});
